# Coding Exercise for Swrve

The application should include the following features:
  ● A control to show latest Cats or Dog results (no search field needed).
  ● Results view with paging of 25 GIFs per page.
  ● A detail view which shows the selected GIF when clicked/hovered/interacted with.


### Prerequisites

Node v6.5.0^

### Installing

To start the project please:
* 1. "cd" into the project
* 2. "yarn install" or "npm install"
* 3. "yarn start" or "npm start"

## Running the tests

1. "yarn test" or "npm test"
2. "a" meaning all the tests


### Break down into end to end tests

* Repsponsiveness test
* Click Refresh button for updating data.

## Deployment

*  The app is deployed on Heroku: "https://buildit-weather-app.herokuapp.com/"
*  Instructions: https://gist.github.com/mars/5e01bb2a074594b44870cb087f54fe2f

## Implementation Plan

1. Integrate translation module using the api
2. Implement search of the location/city, and display weather for it.
3. Include google map and get the weather for clicked location by lat, longitutde.
4. Work on design.
5. Include animated weather images.

## Authors

* **Alexandr Zavalii** - [alexandrzavalii](https://github.com/alexandrzavalii)
 

use axios to cancel requests
use redux
write more tests